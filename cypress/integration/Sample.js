
import cloud from './Function/Cloud'

describe('Sample file', function () {
	const Cloud = new cloud()

	var folder = 'Employee'
	var fileName = 'Employee.xlsx'

	beforeEach(function () {
		cy.getCookies()
	})
		
	it('Login to Cloud & select Company', function () {
		cy.login()
		cy.changeCompany();
	})

	it('Add Employee', function () {
		var sheetName = 'Employee'
		Cloud.addEmployee(folder, fileName, sheetName)
	})

	it('Set Self Service Roll', function () {
		var sheetName = 'SelfServiceRole'
		Cloud.SetSelfServiceRoll(folder, fileName, sheetName)
	})

	it('Set Manager From Approval Matrix', function () {
		var sheetName = 'ApprovalMatrix'
		Cloud.SetManagerFromApprovalMatrix(folder, fileName, sheetName)
	})

	var module = 'Leave'
	var role = 'User'

	it('Set Roll Allocation', function () {
		Cloud.SetRollAllocation(module, role)
	})

	it('Set generate password settings for all employee', function () {
		Cloud.SetGeneratePasswordSettingsForAllEmployee()
	})
	













})
