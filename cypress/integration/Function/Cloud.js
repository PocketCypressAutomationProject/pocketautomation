class Cloud {

    verifyNotication(message) {
        cy.get(".toast-message").invoke('text').should('eq', message)
        cy.get(".toast-message").click({ force: true })
    }

    selectEmployee(employeeId) {
        cy.get('#select2-approvalManager-container').click({ force: true })
        cy.get('input[type="search"]').click({ force: true }).type(employeeId)
        cy.get('.select2-results__option--highlighted').click({ force: true })
    }

    verifyPageTitle(title) {
        cy.get('.card-header').invoke('text').should('contain', title)
    }

    SetRollAllocation(module, role) {
        cy.visit(Cypress.env('url') + 'Settings/Employee/ESSIndex?module=Profile&submodule=RoleAllocation')
        cy.wait(2000)
        cy.get('#drpModule').select(module)
        cy.wait(1000)
        cy.get('#drpRole').select(role)
        cy.wait(1000)
        cy.xpath("//button[contains(text(),'Search')]").click()
        cy.wait(1000)
        cy.get('[class="chk menu"]').each(function (row, i) {
            cy.get('[class="chk menu"]').eq(i).click({ force: true })
        })
        //click on save button
        cy.get('#savedata').click()
        //verify success message
        cy.get(".toast-message").invoke('text').then((text) => {
            expect(text.trim()).equal('Setting Save successfully')
        })
        //click on message to close message box
        cy.get(".toast-message").click()
        cy.wait(1000)
    }

    addEmployee(folder, fileName, sheetName) {

        cy.task('readXlsx', { file: 'cypress/fixtures/' + folder + '/' + fileName, sheet: sheetName }).then((rows) => {
            cy.writeFile('cypress/fixtures/' + folder + '/' + sheetName + ".json", { rows })
        })

        cy.fixture(folder + '/'+ sheetName).then((excelData) => {
					excelData.rows.forEach((data, index) => {

				cy.visit(Cypress.env('url') + 'Employee/Employee/EmployeeList')
				cy.wait(5000)

				cy.xpath("//label[contains(text(),'Employee Wizard')]").click({ force: true })
				cy.wait(10000)


				cy.get('input[name=code]').type(data.EmpCode.trim())

				cy.get('input[name=fname]').type(data.FirstName.trim())

				cy.get('input[name=lname]').type(data.LastName.trim())


				cy.get('#Male').check(data.Gender, { force: true })

				cy.get('select[name=category]').select(data.Category, { force: true })

				cy.get('select[name=ptlocation]').select(data.ProfTaxLocation)


				cy.get('#txt_dateofbirth').click().then(input => {
					input[0].dispatchEvent(new Event('input', { bubbles: true }))
					input.val(data.DateOfBirth)
				})


				cy.get('#txt_dateofjoining').then(input => {
					input.val(data.DateOfJoining)
				})


				cy.get('[for="Date of Joining"]').click()
				cy.wait
				cy.get('select[name=esilocation]').select(data.ESILocation, { force: true })
				cy.get('select[name=metro]').select(data.Metro_TDS, { force: true })
				cy.get('select[name=esidispensary]').select(data.ESIDispensary, { force: true })

				cy.get('#btnSaveBasicDetail').click({ force: true })
				cy.wait(1000)
				cy.get(".toast-message").invoke('text').then((text) => {
					//pass 
					expect(text.trim()).equal('Basic Details Records Saved Successfully.!')
					cy.log(text.trim())
				})
				cy.get(".toast-message").click({ force: true })

                  cy.get('#myModal > .modal-dialog > .modal-content > .modal-header > .col-md-1 > .close').click({ force: true })
                        cy.wait(5000)
			})
		})
    }

	SetGeneratePasswordSettingsForAllEmployee() {
    cy.visit(Cypress.env('url') + 'Settings/Employee/Index?module=hr&submodule=GeneratePassword')
    //cy.xpath("//label[contains(text(),'Category')]").click()
    cy.wait(2000)
    //cy.get('#catall').click()
    cy.get('th > .col-sm-12 > .checkbox > label').click()
    cy.wait(2000)
    cy.get('#OverWriteRad').click({ force: true })
    cy.wait(2000)
    cy.get('[name="PayslipPassword"]').eq(2).click({ force: true })
    cy.wait(2000)
    cy.get('#savesetting').click()
    cy.wait(2000)
    cy.get(".alert-warning").invoke('text').then((text) => {
        cy.log(text.trim())
        expect(text.trim()).contains('Generate Password will get processed in background.')
        cy.wait(2000)
    })
    cy.visit(Cypress.env('url') + 'Settings/Employee/ESSIndex?module=Profile&submodule=PasswordReset')
    cy.get('[class="selection"]').click().type('L1')
    cy.wait(3000)
    cy.get('#select2-multiEmp-results').click()
    cy.wait(2000)
    cy.get('[class="selection"]').click().type('L2')
    cy.wait(3000)
    cy.get('#select2-multiEmp-results').click()
    cy.wait(2000)
    cy.get('#btnView').click()
    cy.wait(2000)
    cy.get(':nth-child(1) > [data-title="Action"] > .btn').click()
    cy.xpath("//span[contains(text(),'L1')]").invoke('text').then((text) => {
        expect(text.trim()).equal('L1')
    })
    cy.get(':nth-child(2) > [data-title="Action"] > .btn').click()
    cy.xpath("//span[contains(text(),'L2')]").invoke('text').then((text) => {
        expect(text.trim()).equal('L2')
    })
    cy.wait(5000)
	}

    SetSelfServiceRoll(folder, fileName, sheetName){

		//var sheetName = 'SelfServiceRole'
		//var filePath = 'Employee.xlsx'

        cy.task('readXlsx', { file: 'cypress/fixtures/' + folder + '/' + fileName, sheet: sheetName }).then((rows) => {
            cy.writeFile('cypress/fixtures/' + folder + '/' + sheetName + ".json", { rows })
        })

        cy.fixture(folder + '/' + sheetName).then((excelData) => {
			excelData.rows.forEach((data, index) => {

				cy.navigate_EmployeeProfile(data.EmpCode)
				cy.get('#profile_detail_tab').click({ force: true })
				cy.wait(1000)
				cy.get('#Profile_SelfServiceRole').click({ force: true })
				cy.wait(2000)
				cy.get('#SelfServiceRole').select(data.Role)
				cy.get('[onclick="saveSelfServiceRole(this)"]').click({ force: true })
			})
		})
	}

    SetManagerFromApprovalMatrix(folder, fileName, sheetName) {
      

        cy.task('readXlsx', { file: 'cypress/fixtures/' + folder + '/' + fileName, sheet: sheetName }).then((rows) => {
            cy.writeFile('cypress/fixtures/' + folder + '/' + sheetName + ".json", { rows })
        })

        cy.fixture(folder + '/' + sheetName).then((excelData) => {
            excelData.rows.forEach((data, index) => {

                cy.navigate_EmployeeProfile(data.EmployeeCode)
                cy.wait(2000)
                cy.get('#approval_matrix_tab').click({ force: true })
                cy.wait(6000)
                if (data.Priority == 1) {
                    cy.get('[title="Add Approval Matrix Manager"]').eq(0).click({ force: true })
                }
                else {
                    cy.get('#approvalComponentTitle > .row > .col-8 > [onclick=""] > .fas').click({ force: true })
                    cy.wait(3000)
                }
                cy.get('#Priority').click({ force: true })
                cy.get('#Priority').clear().type(data.Priority)
                cy.wait(2000)
                cy.get('#select2-approvalManager-container').click({ force: true })
                cy.wait(2000)
                cy.get('input[type="search"]').click({ force: true })
                cy.get('input[type="search"]').type(data.LeaderCode)
                cy.wait(2000)
                cy.contains('li', '[' + data.LeaderCode + ']').click({ force: true })
                //cy.get('.select2-results__option--highlighted').click({ force: true })
                cy.wait(2000)
                if (data.ApprovalMust != '') {
                    cy.get('#approvalmust').select('Yes')
                }
                if (data.ApprovedCancelRights != '') {
                    cy.get('#cancelrights').select('Yes')
                }
                //cy.xpath("//label[contains(text(),'Daily Working Hours')]").click()
                cy.get('#' + data.ModuleName + '').click({ force: true })
                //cy.xpath("//label[contains(text(),'On Duty')]").click()
                cy.get('#btnSaveText').click()
                cy.wait(2000)
            })
        })
    }

}

export default Cloud